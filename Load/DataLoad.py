import json
from csv import DictReader

import pandas
from dateutil import parser
from datetime import datetime, timedelta
from zoneinfo import ZoneInfo
from influxdb import InfluxDBClient


def input_file_path_or_default(file_name: str, default_path: str) -> str:
    path = input(f'Input {file_name} file path or press "Enter" to use default path ({default_path}) -> ')
    if path:
        return path
    return default_path


def parse_date(date_string: str) -> datetime:
    date_string = date_string.strip()
    date = parser.parse(date_string)
    if not date.tzinfo:
        date.astimezone(ZoneInfo('Etc/UTC'))
    return date


def get_df_size(df: pandas.DataFrame) -> int:
    return len(df.index)


def get_slot(date_from: datetime, date_to: datetime) -> dict:
    return {'from': date_from, 'to': date_to}


def get_point(measurement_name: str, time: datetime, data_fields: dict) -> dict:
    return {'measurement': measurement_name, 'time': time, 'fields': data_fields}


config_path = input_file_path_or_default(file_name='Config', default_path='Configs/DataLoadConfig.json')
with open(config_path) as config_file:
    config: dict = json.load(config_file)

csv_path: str = input_file_path_or_default(file_name='CSV', default_path='DataSources/Source.csv')
with open(csv_path) as csv_file:
    reader = DictReader(csv_file, delimiter=';')
    data = {
        'employee_id': [],
        'working_time_from': [],
        'working_time_to': [],
        'dinner_time_from': [],
        'dinner_time_to': [],
        'total_working_time': []
    }
    for row in reader:
        employee_id: str = row['EmployeeId']
        employee_id = employee_id.strip()
        if not employee_id:
            print(f'No Employee ID in row => {row}')
            continue
        try:
            working_time_from = parse_date(row['WorkingTimeFrom'])
            working_time_to = parse_date(row['WorkingTimeTo'])
            dinner_time_from_str = str(row['DinnerTimeFrom']).strip()
            dinner_time_to_str = str(row['DinnerTimeTo']).strip()
            if not dinner_time_from_str and not dinner_time_to_str:
                if working_time_from < working_time_to:
                    total_working_time = working_time_to - working_time_from
                    data['dinner_time_from'].append(None)
                    data['dinner_time_to'].append(None)
                else:
                    print(f'Incorrect working timestamps in row => {row}')
                    continue
            else:
                try:
                    dinner_time_from = parse_date(dinner_time_from_str)
                    dinner_time_to = parse_date(dinner_time_to_str)
                    if working_time_from < dinner_time_from < dinner_time_to < working_time_to:
                        total_working_time = (dinner_time_from - working_time_from) + (working_time_to - dinner_time_to)
                        data['dinner_time_from'].append(dinner_time_from)
                        data['dinner_time_to'].append(dinner_time_to)
                    else:
                        print(f'Incorrect timestamps in row => {row}')
                        continue
                except ValueError:
                    print(f'Value error while reading dinner timestamps in row => {row}')
                    continue
            data['employee_id'].append(employee_id)
            data['working_time_from'].append(working_time_from)
            data['working_time_to'].append(working_time_to)
            data['total_working_time'].append(total_working_time)
        except ValueError:
            print(f'Value error while reading working timestamps in row => {row}')
            continue

data_frame = pandas.DataFrame(data)
print(f'Data frame with {get_df_size(data_frame)} records was created.')
total_working_time_data = data_frame['total_working_time']
q1 = total_working_time_data.quantile(0.25)
q3 = total_working_time_data.quantile(0.75)
d = 1.5 * (q3 - q1)
data_frame = data_frame[(total_working_time_data > q1 - d) & (total_working_time_data < q3 + d)]
print(f'Data frame was cleaned up - {get_df_size(data_frame)} records remained.')

data_set = dict()
for i, row in data_frame.iterrows():
    key: str = row['employee_id']
    if pandas.isnull(row['dinner_time_from']) or pandas.isnull(row['dinner_time_to']):
        new_slots = [get_slot(row['working_time_from'], row['working_time_to'])]
    else:
        new_slots = [
            get_slot(row['working_time_from'], row['dinner_time_from']),
            get_slot(row['dinner_time_to'], row['working_time_to'])
        ]
    slots: list = data_set.get(key)
    if slots:
        slots.extend(new_slots)
    else:
        data_set[key] = new_slots

print('Connecting to InfluxDB...')
influx_config = config['Influx']
influx_client = InfluxDBClient(
    host=influx_config['Host'],
    port=influx_config['Port'],
    username=influx_config['Username'],
    password=influx_config['Password']
)
databases = influx_client.get_list_database()
print('Connection to InfluxDB was created.')
database = influx_config['Database']
for db in databases:
    if db['name'] == database:
        break
else:
    influx_client.create_database(database)
    print(f'New "{database}" database was created.')
influx_client.switch_database(database)
print(f'Database was switched to "{database}"')

periods_load_settings: dict = config['PeriodsLoad']
if periods_load_settings['IsEnabled']:
    print('Loading data as periods...')
    measurement: str = periods_load_settings['Measurement']
    points = []
    for key, time_slots in data_set.items():
        for slot in time_slots:
            coming_point = get_point(measurement, slot['from'], {key: 1})
            leaving_point = get_point(measurement, slot['to'], {key: 0})
            points.append(coming_point)
            points.append(leaving_point)
    is_success = influx_client.write_points(points)
    if not is_success:
        raise "DATA LOAD ERROR"
    print('Data were loaded as periods.')
else:
    print('Periods Load is not required.')

series_load_settings: dict = config['SeriesLoad']
if series_load_settings['IsEnabled']:
    print('Loading data as series...')
    measurement: str = series_load_settings['Measurement']
    load_date_from = parser.isoparse(series_load_settings['DateFrom'])
    load_date_to = parser.isoparse(series_load_settings['DateTo'])
    time_span = timedelta(seconds=series_load_settings['Span'])
    current_time = load_date_from
    while current_time < load_date_to:
        fields = dict()
        for key, time_slots in data_set.items():
            for slot in time_slots:
                if slot['from'] <= current_time <= slot['to']:
                    fields[key] = 1
                    break
            else:
                fields[key] = 0
        point = get_point(measurement, current_time, fields)
        is_success = influx_client.write_points([point])
        if not is_success:
            raise "DATA LOAD ERROR"
        current_time += time_span
    else:
        print('Data were loaded as series.')
else:
    print('Series Load is not required.')
