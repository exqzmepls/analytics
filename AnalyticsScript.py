from influxdb import InfluxDBClient
import numpy as np
import pandas as pd
pd.set_option('display.max_rows', 999)

config_path = "Load/Configs/DefaultConfig.json"
database_name = "SmartOffice"
measurement_name = "activity"

client = InfluxDBClient()
client.switch_database(database_name)
result = client.query(f'SELECT * FROM "{measurement_name}"')

series = result.raw['series']

for seria in series:
    if seria['name'] == measurement_name:
        columns = seria['columns']
        values = seria['values']
        break
finalDict = {"persone_ID": np.NaN, "time": np.NaN, "Active": np.NaN}
i = 1
val = np.array(values)
for column in columns:
    if column == 'time':
        continue
    if i == 1:
        finalDict['persone_ID'] = np.full((len(values)), column)
        finalDict['time'] = val[:, 0]
        finalDict['Active'] = val[:, i]
    else:
        finalDict['persone_ID'] = np.concatenate((finalDict['persone_ID'], np.full((len(values)), column)), axis=0)
        finalDict['time'] = np.concatenate((finalDict['time'], val[:,0]), axis=0)
        finalDict['Active'] = np.concatenate((finalDict['Active'], val[ :,i]), axis=0)
    i += 1
df = pd.DataFrame(finalDict)
df['time'] = pd.to_datetime(df['time'])
df['time'] = df['time'].dt.strftime("%H:%M:%S")
df['Active'] = df['Active'].astype('int64')
df['persone_ID'] = df['persone_ID'].astype('int64')
dataFilter = df.groupby(['persone_ID','time'])['Active'].sum().groupby(['persone_ID']).sum().to_frame()
Q1 = dataFilter['Active'].quantile(0.10)
Q3 = dataFilter['Active'].quantile(0.90)
IQR = Q3 - Q1
Filter = (dataFilter['Active'] >= Q1 - 1.5 * IQR) & (dataFilter['Active'] <= Q3 + 1.5 *IQR)
Filter = Filter.to_frame()
Filter_indexes = np.array(Filter[Filter['Active']].index)
df = df[df['persone_ID'].isin(Filter_indexes)]
mean_work_hours_every_workers = df.groupby(['persone_ID','time']).mean().groupby(['persone_ID'])['Active'].sum()
mean_work_hours = mean_work_hours_every_workers.mean()
sum_work_hours = df.groupby(['persone_ID','time'])['Active'].sum().groupby(['persone_ID']).sum()
max_id = sum_work_hours.idxmax()
min_id = sum_work_hours.idxmin()
_max = df[df['persone_ID'] == max_id].groupby(['persone_ID','time']).sum().groupby(['persone_ID'])['Active'].sum()
_min = df[df['persone_ID'] == min_id].groupby(['persone_ID','time']).sum().groupby(['persone_ID'])['Active'].sum()

for (person, time, active) in zip(df['persone_ID'].tolist(),df['time'].tolist(),df['Active'].tolist()):
    data = {'measurement': "FilteredData", 'tags': {}, 'fields': {}}
    data['fields']['persone_ID'] = float(person)
    data['fields']['time'] = time
    data['fields']['Active'] = float(active)
    client.write_points([data])
data_set = mean_work_hours_every_workers.to_dict()
for (person, mean_hours) in zip(data_set.keys(), data_set.values()):
    data = {'measurement': "MeanWorkHours", 'tags': {}, 'fields': {}}
    data['fields']['persone_ID'] = float(person)
    data['fields']['mean_hours'] = mean_hours
    client.write_points([data])
data_set = sum_work_hours.to_dict()
for (person, sum_hours) in zip(data_set.keys(), data_set.values()):
    data = {'measurement': "SumHours", 'tags': {}, 'fields': {}}
    data['fields']['persone_ID'] = float(person)
    data['fields']['sum_hours'] = sum_hours
    client.write_points([data])
data = {'measurement': "MinMaxMeanHours", 'tags': {}, 'fields': {}}
data['fields']['max'] = _max
data['fields']['min'] = _min
data['fields']['mean'] = mean_work_hours
client.write_points([data])
